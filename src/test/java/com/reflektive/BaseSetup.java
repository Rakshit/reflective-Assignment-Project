package com.reflektive;

import com.reflektive.util.ConstantUtil;
import org.testng.annotations.BeforeSuite;
import util.ExcelUtil;
import util.LoggerUtils;
import util.PropertiesUtil;

import java.util.List;

public class BaseSetup {

    public static PropertiesUtil prop ;
    public static ExcelUtil excelUtil ;

    @BeforeSuite
    public void baseSuite(){
        prop = new PropertiesUtil("config/config.properties");
        excelUtil = new ExcelUtil(System.getProperty("user.dir") + "/"+ prop.getProperty("filePath"), prop.getProperty("sheetName") ,
                prop.getProperty("fileName")) ;

    }

//    public static void main(String[] args){
//        new BaseSetup().baseSuite();
//        excelUtil.addExpenseToExcel("1" , 2.0);
//        excelUtil.addExpenseToExcel("2" , 2.0);
//        excelUtil.addExpenseToExcel("3" , 2.0);
//        excelUtil.addExpenseToExcel("4" , 2.0);
//        LoggerUtils.info("sum : " +excelUtil.readExceldataForSum());
//
//    }

    public void waitFor(int timeInMill){
        try {
            Thread.sleep(timeInMill);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
