package com.reflektive;

import org.testng.Assert;
import org.testng.annotations.Test;
import util.LoggerUtils;

public class ExcelSumTest extends BaseSetup {


    /* This test is dependent on other test execution as the data inserted by them
       Checking sum of all the expenses and validating

    */
    @Test
    public void checkTotalAmoutOfExpense() {
        LoggerUtils.info("Executing Test : " + Thread.currentThread().getStackTrace()[1].getMethodName());
        LoggerUtils.info("Validating Data in Expense sheet....");
        Double sum = excelUtil.readExceldataForSum();
        Assert.assertTrue(sum != 0.0 , "No Expense Added in th sheet");
        LoggerUtils.info("Total Expense : " + sum);


    }
}
