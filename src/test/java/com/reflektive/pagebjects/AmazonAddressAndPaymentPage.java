package com.reflektive.pagebjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class AmazonAddressAndPaymentPage {
    WebDriver driver;
    WebDriverWait wait;


    public AmazonAddressAndPaymentPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);

    }


    public void selectAddress() {
        if (isElementPresent(By.xpath("//*[@id=\"address-book-entry-0\"]"))) {
            driver.findElement(By.xpath("//*[@id=\"address-book-entry-0\"]")).findElement(By.tagName("a")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("marketing")));

        }

    }

    public void selectPaymentMode() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        waitFor(2000);
        WebElement mode = driver.findElement(By.id("net-banking"));
        mode.click();
        WebElement netBnk = driver.findElement(By.xpath("//div[@id='net-banking']/div[2]/div[2]/div/span/span/span/button/span"));
        netBnk.click();
        netBnk.findElement(By.xpath("//a[contains(text(),'Axis Bank')]")).click();
    }


    public void continuePayment() {
        waitFor(3000);
        driver.findElement(By.id("continue-top")).click();
    }

    public boolean isContinueAvailable(){
        return isElementPresent(By.id("active-bank-content")) ;
    }


    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean reviewOrder() {
        if(!isElementPresent(By.id("spc-top"))){
            return false ;
        }
        return true ;
    }

    public void makePayment() {
        driver.findElement(By.xpath("//*[@id=\"order-summary-box\"]/div[1]/div/div[1]/div/span/span/input")).click();
    }


    public String getFinalAmount() {
        WebElement amount = driver.findElement(By.cssSelector("#subtotals-marketplace-table > table > tbody > tr:nth-child(4) > td.a-size-medium.a-color-price.aok-align-bottom.aok-nowrap.grand-total-price.a-text-right > strong > span"));
        return amount.getText() ;
    }

    public void waitFor(int timeInMill){
        try {
            Thread.sleep(timeInMill);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
