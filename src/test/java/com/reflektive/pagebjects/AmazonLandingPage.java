package com.reflektive.pagebjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AmazonLandingPage {
    WebDriver driver;
    WebDriverWait wait;


    public AmazonLandingPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);

    }

    public void searchText(String bookName){
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys(bookName);
        driver.findElement(By.xpath("//*[@id=\"nav-search\"]/form/div[2]/div/input")).click();
    }
}
