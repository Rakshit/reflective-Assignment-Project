package com.reflektive.pagebjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class AmazonBookPage {
    WebDriver driver;
    WebDriverWait wait;


    public AmazonBookPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);

    }

    public void selectBookFormat() {
        List<WebElement> formatList = driver.findElements(By.xpath("//*[@id=\"tmmSwatches\"]/ul/li"));
        Assert.assertNotNull(formatList.size() != 0);
        for (WebElement element : formatList) {
            if (element.findElement(By.tagName("a")).findElement(By.tagName("span")).getText().equalsIgnoreCase("PaperBack") && !element.getAttribute("class").split(" ")[1].equalsIgnoreCase("selected")) {
                element.click();
            }
        }


    }

    public void clickBuyNow() {
        driver.findElement(By.id("buy-now-button")).click();

    }

    public boolean isLoggedIn() {
        return !isElementPresent(By.id("ap_email"));
    }


    public void loginAccount(String userName, String Password) {
        driver.findElement(By.id("ap_email")).sendKeys(userName);
        driver.findElement(By.id("continue")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("ap_password")));
        driver.findElement(By.id("ap_password")).sendKeys(Password);
        driver.findElement(By.id("signInSubmit")).click();
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
