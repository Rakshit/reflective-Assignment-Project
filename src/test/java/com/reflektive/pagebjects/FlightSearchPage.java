package com.reflektive.pagebjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class FlightSearchPage {
    WebDriver driver;
    WebDriverWait wait;

    public FlightSearchPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        flightSearchSelection();

    }

    public void flightSearchSelection() {
        driver.findElement(By.linkText("Flights")).click();
    }

    public void searchForReturnJourney(String sourceCity , String DesCity) {
        selectTwoWayJourney();
        enterOriginAs(sourceCity);
        selectTheFirstAvailableAutoCompleteOptionForOrigin();
        enterDestinationAs(DesCity);
        selectTheFirstAvailableAutoCompleteOptionForDestination();
        enterDepartureDate();
        enterReturnDate();
        selectChilrenDrop();
        //all fields filled in. Now click on search
        searchForTheJourneyAndWaitForSearchCompletion();
    }

    private void selectChilrenDrop() {
        WebElement drop = driver.findElement(By.xpath("//*[@id=\"Childrens\"]"));
        drop.click();
        Select dropdown = new Select(drop);
        dropdown.selectByIndex(1);

    }

    private void selectTheFirstAvailableAutoCompleteOptionForOrigin() {
        //select the first item from the auto complete list
        wait.until(ExpectedConditions.elementToBeClickable(By.id("ui-id-1")));
        List<WebElement> optionsList = driver.findElement(By.id("ui-id-1")).findElements(By.tagName("li"));
        optionsList.get(0).click();

    }


    public String departDate() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 14);
        return new SimpleDateFormat("dd/MM/yyyy").format(c.getTime());
    }

    public String returnDate() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 15);
        return new SimpleDateFormat("dd/MM/yyyy").format(c.getTime());
    }

    private void searchForTheJourneyAndWaitForSearchCompletion() {
        wait.until(ExpectedConditions.elementToBeClickable(By.id("SearchBtn")));
        driver.findElement(By.id("SearchBtn")).click();
        waitFor(5000);
        if (!isElementPresent(By.className("searchSummary"))) {
            waitFor(5000);
        }

    }


    private void enterDepartureDate() {
        WebElement element = driver.findElement(By.xpath("//*[@id=\"DepartDate\"]"));
        element.sendKeys(departDate());

    }


    private void enterReturnDate() {
        WebElement element = driver.findElement(By.xpath("//*[@id=\"ReturnDate\"]"));
        element.sendKeys(returnDate());

    }


    private void enterDestinationAs(String destination) {
        driver.findElement(By.id("ToTag")).clear();
        driver.findElement(By.id("ToTag")).sendKeys(destination);
    }


    private void enterOriginAs(String origin) {
        driver.findElement(By.id("FromTag")).clear();
        driver.findElement(By.id("FromTag")).sendKeys(origin);
    }


    private void selectTwoWayJourney() {
        driver.findElement(By.id("RoundTrip")).click();
    }


    private void selectTheFirstAvailableAutoCompleteOptionForDestination() {

        //select the first item from the auto complete list
        wait.until(ExpectedConditions.elementToBeClickable(By.id("ui-id-2")));
        List<WebElement> optionsList = driver.findElement(By.id("ui-id-2")).findElements(By.tagName("li"));
        optionsList.get(0).click();
    }


    private void waitFor(int durationInMilliSeconds) {
        try {
            Thread.sleep(durationInMilliSeconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

}
