package com.reflektive.pagebjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.LoggerUtils;

import java.util.concurrent.TimeUnit;

public class SearchResultPage {

    WebDriver driver;
    WebDriverWait wait;
    Actions action;

    public SearchResultPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        action = new Actions(driver);
    }

    public boolean resultsAppearForInboundJourney() {
        waitForSearchResultsToAppear();
        return isElementPresent(By.xpath("//*[@id=\"flightForm\"]/section[2]/div[4]/div[1]/nav"));
    }

    public boolean resultsAppearForOutboundJourney() {
        waitForSearchResultsToAppear();
        return isElementPresent(By.xpath("//*[@id=\"flightForm\"]/section[2]/div[4]/div[2]/nav"));
    }


    private void waitForSearchResultsToAppear() {
        //Conditional wait for one of the elements on the search results page to be present
        wait.until(ExpectedConditions.elementToBeClickable(By.className("searchSummary")));
    }


    public boolean departFlightSelected() {
        try {

            WebElement departList = driver.findElement(By.xpath("//*[@id=\"flightForm\"]/section[2]/div[4]/div[1]"));
            WebElement sorter = departList.findElement(By.xpath("//*[@id=\"sorterTpl\"]"));
            // check order of the flight is by price
            if (!sorter.findElement(By.className("price")).findElement(By.tagName("a")).getAttribute("class").split(" ")[1].equalsIgnoreCase("sortAsc")) {
                sorter.findElement(By.className("price")).click();
            }
            departList.findElement(By.xpath("//*[@id=\"flightForm\"]/section[2]/div[4]/div[1]/nav/ul/li[1]/div")).click();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }


    }

    public boolean isLastReturnFlightSelected() {
        try {
            WebElement returnFlightList = driver.findElement(By.xpath("//*[@id=\"flightForm\"]/section[2]/div[4]/div[2]"));
            WebElement sorter = returnFlightList.findElement(By.id("sorterTpl"));
            //check order of the flight is by duration
            WebElement element = sorter.findElement(By.className("depart")).findElement(By.tagName("a"));
            action.moveToElement(element).click().perform();
            while (element.getAttribute("class").split(" ")[1].equalsIgnoreCase("sortAsc")) {
                action.moveToElement(element).click().perform();
            }
            returnFlightList.findElement(By.xpath("//*[@id=\"flightForm\"]/section[2]/div[4]/div[2]/nav/ul/li[1]/div")).click();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public FlightReviewDetailPage clickBookFlightAndNavigateToReviewPage() {
        try {
            wait.until((ExpectedConditions.visibilityOfElementLocated(By.xpath("(//button[@type='submit'])[4]"))));
            WebElement book = driver.findElement(By.xpath("(//button[@type='submit'])[4]"));
            book.click();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new FlightReviewDetailPage(driver);

    }


    public String getTotalFare() {
        WebElement fare = driver.findElement(By.xpath("//*[@id=\"flightForm\"]/section[2]/div[3]/div[1]/h2"));
        fare.click();
        return fare.getText();
    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
