package com.reflektive.pagebjects;

import com.reflektive.BaseSetup;
import com.reflektive.util.Scroll;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class FlightReviewDetailPage extends BaseSetup {
    WebDriver driver;
    WebDriverWait wait;


    public FlightReviewDetailPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);

    }

    public boolean verifyBookingDetailPage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated((By.cssSelector("p.pageTitle"))));
        return isElementPresent(By.cssSelector("p.pageTitle"));
    }

    public void addtravellersDetail() {
        new Scroll(driver).ByPixel("418");
        waitFor(2000);
        WebElement insuranceBox = driver.findElement(By.id("insurance_box"));
        if (insuranceBox.isSelected()) {
            insuranceBox.click();
        }
        driver.findElement(By.id("itineraryBtn")).click();
        addAdultDetails();
        addChildsDetails();
        driver.findElement(By.id("travellerBtn")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

    }


    public void proceedForPaymentDetail() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("NBTab")));
        driver.findElement(By.id("NBTab")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("axis_bank")));
        driver.findElement(By.id("axis_bank")).click();
    }

    public void submitPayment() {
        new Scroll(driver).ByPixel("37");
        driver.findElement(By.id("paymentSubmit")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("paymentSubmit")));
    }


    public String totalFare() {
        waitFor(2000);
        String fare = driver.findElement(By.xpath("//*[@id=\"totFareCV\"]")).findElement(By.id("counter")).getText();
        return fare != null ? fare : "";
    }

    public void addAdultDetails() {
        waitFor(3000);
        driver.findElement(By.id("username")).sendKeys(prop.getProperty("user"));
        driver.findElement(By.id("LoginContinueBtn_1")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("AdultTitle1")));

        //Enter Name
        selectoption("AdultTitle1", "Mr");
        driver.findElement(By.id("AdultFname1")).sendKeys(prop.getProperty("Fname"));
        driver.findElement(By.id("AdultLname1")).sendKeys(prop.getProperty("Lname"));

        //Enter b'date if present
        if (isElementPresent(By.id("AdultDobDay1"))) {
            selectoption("AdultDobDay1", "16");
            selectoption("AdultDobMonth1", "11");
            selectoption("AdultDobYear1", "1990");
        }
        // Enter Phone
        driver.findElement(By.id("mobileNumber")).sendKeys(prop.getProperty("mobileNumber"));

        // select Nationality
        selectNationality("(//input[@type='text'])[5]");

    }

    public void addChildsDetails() {
        Select traveler = new Select(driver.findElement(By.id("ChildTitle1")));
        traveler.selectByValue("Miss");
        driver.findElement(By.id("ChildFname1")).sendKeys(prop.getProperty("DFname"));
        driver.findElement(By.id("ChildLname1")).sendKeys(prop.getProperty("Lname"));

        if (isElementPresent(By.id("ChildDobDay1"))) {
            selectoption("ChildDobDay1", "23");
            selectoption("ChildDobMonth1", "10");
            selectoption("ChildDobYear1", String.valueOf(Calendar.getInstance().get(Calendar.YEAR) - 10));
        }
        // select nationality if present
        selectNationality("(//input[@type='text'])[9]");
    }

    public void selectoption(String id, String value) {
        Select select = new Select(driver.findElement(By.id(id)));
        select.selectByValue(value);
    }

    public void selectNationality(String xpath) {
        if (isElementPresent(By.xpath(xpath))) {
            WebElement nationality = driver.findElement(By.xpath(xpath));
            if (nationality.isDisplayed()) {
                nationality.sendKeys(prop.getProperty("nationality"));
                driver.findElement(By.xpath(xpath)).sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
            }

        }
    }


    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

}
