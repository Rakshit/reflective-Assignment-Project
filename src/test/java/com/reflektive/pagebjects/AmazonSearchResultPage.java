package com.reflektive.pagebjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.List;

public class AmazonSearchResultPage {
    WebDriver driver;
    WebDriverWait wait;


    public AmazonSearchResultPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);

    }

    public String selectBookAndSwitchWindow(String text){
        List<WebElement> anchors = driver.findElements(By.tagName("a"));
        Iterator<WebElement> i = anchors.iterator();
        String link = null;
        while(i.hasNext()) {
            WebElement anchor = i.next();
            if(anchor.getAttribute("title").contains(text)) {
                link =  anchor.getAttribute("href");
                break;
            }
        }
        return link ;
    }
}
