package com.reflektive.util;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class Scroll {

    WebDriver driver;
    public Scroll(WebDriver driver){
        this.driver = driver ;
    }

    public void ByPixel(String pixels) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        // This  will scroll down the page by  1000 pixel vertical
        js.executeScript("window.scrollBy(0,"+ pixels+")");
    }
}