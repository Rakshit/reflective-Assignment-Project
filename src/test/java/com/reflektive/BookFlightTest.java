package com.reflektive;

import com.reflektive.pagebjects.FlightReviewDetailPage;
import com.reflektive.pagebjects.FlightSearchPage;
import com.reflektive.pagebjects.SearchResultPage;
import com.reflektive.util.ConstantUtil;
import driver.AppDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import util.LoggerUtils;

import java.util.concurrent.TimeUnit;

public class BookFlightTest extends BaseSetup {

    private WebDriver driver;
    WebDriverWait wait;
    FlightSearchPage flightSearchPage;
    SearchResultPage searchResultPage;

    @BeforeClass
    public void initializeDriver() {
        driver = new AppDriver(prop.getProperty("browser")).getDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10);
        driver.get(ConstantUtil.CLEARTRIP_URL);
        flightSearchPage = new FlightSearchPage(driver);
        searchResultPage = new SearchResultPage(driver);
    }

    @Test()
    public void bookFlightForKaranTest() {
        LoggerUtils.info("Executing Test : " + Thread.currentThread().getStackTrace()[1].getMethodName());
        //Search Flight with source and destination
        LoggerUtils.info("Booking Flight From : " +prop.getProperty("srcCity")) ;
        LoggerUtils.info("Booking Flight To : " +prop.getProperty("desCity")) ;
        flightSearchPage.searchForReturnJourney(prop.getProperty("srcCity"), prop.getProperty("desCity"));
        //verify that result appears for the provided journey search
        Assert.assertTrue(searchResultPage.resultsAppearForInboundJourney());
        Assert.assertTrue(searchResultPage.resultsAppearForOutboundJourney());

        //select flight from source to destination and vice versa
        Assert.assertTrue(searchResultPage.departFlightSelected());
        LoggerUtils.info("Selected Cheapest Flight From Bang to del");

        Assert.assertTrue(searchResultPage.isLastReturnFlightSelected());
        LoggerUtils.info("And last Flight From del to bangalore");
        searchResultPage.getTotalFare();

        // redirecting to review page
        FlightReviewDetailPage flight = searchResultPage.clickBookFlightAndNavigateToReviewPage();
        Assert.assertTrue(flight.verifyBookingDetailPage(), "Error in landing to review detail page");
        //providing traveller details from config properties
        flight.addtravellersDetail();
        LoggerUtils.info("Travel Details Entered");
        flight.proceedForPaymentDetail();
        LoggerUtils.info("Checking Fare......");
        String totalFare = flight.totalFare() ;
        LoggerUtils.info("Total Flight Fare Expense To be added to Excel: " + flight.totalFare());
        Assert.assertNotEquals(flight.totalFare(), "", "Fare received empty");
        //add expenses to excel
        excelUtil.addExpenseToExcel("Flight Fare", Double.valueOf(totalFare.trim().replace(",", "")));
        // submit payment annd waiting for Payment page to load
        flight.submitPayment();
        Assert.assertFalse(flight.isElementPresent(By.id("paymentSubmit"))); ;

    }

    @AfterClass
    public void closeDriver() {
        //close the browser
        driver.close();
    }

}









