package com.reflektive;

import com.reflektive.pagebjects.*;
import com.reflektive.util.ConstantUtil;
import driver.AppDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import util.LoggerUtils;

import java.util.concurrent.TimeUnit;

public class BuyBookForSandhyaTest extends BaseSetup {
    private WebDriver driver;
    WebDriverWait wait;
    AmazonLandingPage amazonLandingPage;
    AmazonSearchResultPage amazonSearchResultPage;
    AmazonBookPage amazonBookPage;
    AmazonAddressAndPaymentPage amazonAddressAndPaymentPage;

    @BeforeClass
    public void initializeDriver() {
        driver = new AppDriver(prop.getProperty("browser")).getDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10);
        driver.get(ConstantUtil.AMAZON_URL);
        amazonLandingPage = new AmazonLandingPage(driver);
        amazonSearchResultPage = new AmazonSearchResultPage(driver);
        amazonBookPage = new AmazonBookPage(driver);
        amazonAddressAndPaymentPage = new AmazonAddressAndPaymentPage(driver);
    }

    @Test
    public void giftForSandhyaTest() {
        LoggerUtils.info("Executing Test : " + Thread.currentThread().getStackTrace()[1].getMethodName());
        //Search book
        LoggerUtils.info("Search name : "+ ConstantUtil.BOOK_NAME);
        amazonLandingPage.searchText(ConstantUtil.BOOK_NAME);
        //select book from result and navigate to book page
        String bookPageLink = amazonSearchResultPage.selectBookAndSwitchWindow(ConstantUtil.BOOK_NAME);
        // validate link to new page
        Assert.assertTrue(bookPageLink.length() != 0);
        //navigate to book page
        driver.navigate().to(bookPageLink);
        // select paperback edition
        amazonBookPage.selectBookFormat();
        amazonBookPage.clickBuyNow();

        LoggerUtils.info("Is Loggedin : "+ amazonBookPage.isLoggedIn());
        if (!amazonBookPage.isLoggedIn()) {
            amazonBookPage.loginAccount(prop.getProperty("user"), prop.getProperty("password"));
        }

        //select delivery address
        amazonAddressAndPaymentPage.selectAddress();
        //select payment mode
        amazonAddressAndPaymentPage.selectPaymentMode();
        //check if continue is available
        Assert.assertTrue(amazonAddressAndPaymentPage.isContinueAvailable());

        amazonAddressAndPaymentPage.continuePayment();
        Assert.assertTrue(amazonAddressAndPaymentPage.reviewOrder());

        // add expense to excel
        Double giftAmount = Double.valueOf(amazonAddressAndPaymentPage.getFinalAmount().trim().replace(",", ""));
        LoggerUtils.info("Amount For Sandhya's Gift To be added to Excel: " + giftAmount);
        excelUtil.addExpenseToExcel("Gift for Sandhya", giftAmount);

        // redirecting to payment gateway using net-banking as payment mode
        amazonAddressAndPaymentPage.makePayment();

    }

    @AfterClass
    public void closeDriver() {
        //close the browser
        driver.close();
    }


}
