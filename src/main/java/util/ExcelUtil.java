package util;


import java.io.*;
import java.util.*;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

public class ExcelUtil {

    String filePath;
    String sheetName;
    String fileName;
    HSSFWorkbook workbook;


    public ExcelUtil(String filePath, String sheetName, String fileName) {
        this.filePath = filePath;
        this.sheetName = sheetName;
        this.fileName = fileName;
        workbook = createWorkbook(filePath + "/" + fileName);
    }

    private void writeToExcel(List<String> data) {
        try {
            //Access the worksheet, so that we can update / modify it.
            HSSFSheet myWorksheet = workbook.getSheetAt(0);
            // declare a Cell object
            Cell cell = null;
            // Access the cell first to update the value
            int rowCount = myWorksheet.getLastRowNum();
            HSSFRow Currentrow = myWorksheet.createRow(rowCount + 1);
            for (int i = 0; i < data.size(); i++) {
                cell = Currentrow.createCell(i);
                cell.setCellValue(data.get(i));
            }
            //Open FileOutputStream to write updates
            FileOutputStream outputFile = new FileOutputStream(new File(filePath + "/" + fileName));
            //write changes
            workbook.write(outputFile);
            //close the stream
            outputFile.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Double readExceldataForSum() {
        HSSFSheet worksheet = getWorkSheet();
        Double total = 0.0;
        int rowCount = worksheet.getLastRowNum();
        for (int i = 1; i <= rowCount; i++) {
            String value = worksheet.getRow(i).getCell(1).getStringCellValue();
            total += Double.valueOf(value);
        }
        return total;

    }


    public HSSFSheet getWorkSheet() {
        //Read the spreadsheet that needs to be updated
        HSSFSheet workSheet = null;
        if (workbook != null) {
            workSheet = workbook.getSheet(sheetName);
        }
        return workSheet;

    }


    public HSSFWorkbook createWorkbook(String fileName) {
        HSSFWorkbook workbook = null;
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet(sheetName);
            Row row = sheet.createRow(0);
            Cell cell0 = row.createCell(0);
            cell0.setCellValue("Description");
            Cell cell1 = row.createCell(1);
            cell1.setCellValue("Amount");
            workbook.write(fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return workbook;
    }


    public void addExpenseToExcel(String Text, Double value) {
        List<String> addExpenseToExcel = new ArrayList<>();
        addExpenseToExcel.add(Text);
        addExpenseToExcel.add(String.valueOf(value));
        writeToExcel(addExpenseToExcel);
    }

}