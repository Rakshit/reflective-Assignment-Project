package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.concurrent.TimeUnit;

public class AppDriver {

    WebDriver driver;


    public AppDriver(String driverName){
         if(driverName.equalsIgnoreCase("CH")){
             setChromeDriver();
         }else if(driverName.equalsIgnoreCase("FF")){
             setFireFoxDriver();
         }else {
             driver = new InternetExplorerDriver();
         }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    public WebDriver getDriver(){
        return driver;
    }

    public void setChromeDriver(){
        //setup chromedriver
        System.setProperty(
                "webdriver.chrome.driver",
                "webdriver/chromedriver");
        driver = new ChromeDriver();
    }


    public void setFireFoxDriver(){
        System.setProperty(
                "webdriver.gecko.driver",
                "webdriver/geckodriver");
        driver = new FirefoxDriver();

    }
}
