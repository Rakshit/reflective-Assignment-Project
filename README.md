# selenium-test
 test project of selenium using ChromeWebDriver


## Prerequisites ##

* Have [java](http://www.oracle.com/technetwork/java/javase/downloads/index.html) installed
* Have [maven](http://maven.apache.org/) installed

## Execute automation tests ##
   mvn clean test

## Expense Traker Sheet
   OutputDir/ExpenseTraker.xls


## Custom report using Extent Report ##
   test-output/REFLEKTIVE_UI_TEST_REPORT.html
   
## Possible Enhancements
  Retry mechanism to resolve intermitent failure due to network delay getting an element
